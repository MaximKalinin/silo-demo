export interface IFetchData {
  ad: {
    company: string;
    text: string;
    url: string;
  };
  data: IColor[];
  page: number;
  per_page: number;
  total: number;
  total_pages: number;
}

export interface IColor {
  color: string;
  id: number;
  name: string;
  pantone_value: string;
  year: number;
}

export interface IPoint {
  sin: number;
  cos: number;
  r: number;
  startAngle: number;
  speed: number;
}
