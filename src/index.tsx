import React, { FC, useState, useEffect } from "react";
import ReactDOM from "react-dom";
import axios from "axios";
import { IFetchData, IColor } from "@app/model";
import { ColorCard, Modal, ShapePath } from "@app/components";

import styles from "@app/styles.module.less";
import "@app/global.styles.less";

function useFetchData(setData: (data: IColor[]) => void) {
  useEffect(() => {
    const fetchData = async () => {
      const response: { data: IFetchData } = await axios.get(
        "https://reqres.in/api/unknown?per_page=12"
      );
      console.log(response);
      setData(response.data.data);
    };
    fetchData();
  }, []);
}

const App: FC = () => {
  const [data, setData] = useState<IColor[]>([]);
  const [modalIndex, setModalIndex] = useState(-1);
  useFetchData(setData);
  const modalData = data[modalIndex];
  return (
    <>
      <h1 className={styles.header}>Explore colors of the year:</h1>
      {data &&
        data.map(({ id, color, name }, index) => (
          <ColorCard
            onClick={() => setModalIndex(index)}
            name={name}
            color={color}
            key={id}
          />
        ))}
      <Modal open={Boolean(modalData)} onClose={() => setModalIndex(-1)}>
        {modalData && (
          <>
            <svg className={styles.modal__svg} viewBox="0 0 1 1">
              <ShapePath fill={modalData.color} />
            </svg>
            <div>color: {modalData.color}</div>
            <div>name: {modalData.name}</div>
            <div>year: {modalData.year}</div>
            <div>pantone value: {modalData.pantone_value}</div>
          </>
        )}
      </Modal>
    </>
  );
};

ReactDOM.render(<App />, document.getElementById("root"));
