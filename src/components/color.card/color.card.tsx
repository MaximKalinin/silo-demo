import React, { FC, useRef, useEffect } from "react";
import { ShapePath } from "@app/components";

import styles from "./styles.module.less";

interface IColorCardProps {
  color: string;
  name: string;
  onClick?: () => void;
}

export const ColorCard: FC<IColorCardProps> = ({ color, name, onClick }) => {
  const ref = useRef<HTMLDivElement>();
  // const hightlightRef = useRef<HTMLDivElement>();
  useEffect(() => {
    const el = ref.current;
    // const hightlightEl = hightlightRef.current;
    if (!el) return;
    const [middleX, middleY] = (() => {
      const rect = el.getBoundingClientRect();
      return [rect.width / 2, rect.height / 2];
    })();
    const rotationLimit = 3;
    const onMouseEnter = () => {
      el.classList.add(styles.transition);
      // hightlightEl.classList.add(styles.transition);
      setTimeout(() => {
        el.classList.remove(styles.transition);
        // hightlightEl.classList.remove(styles.transition);
      }, 250);
    };
    const onMouseMove = (event: MouseEvent) => {
      const x = event.offsetX;
      const y = event.offsetY;
      const rotateX = (x - middleX) * (rotationLimit / middleX);
      const rotateY = (middleY - y) * (rotationLimit / middleY);
      el.style.transform = `rotateX(${rotateY}deg) rotateY(${rotateX}deg)`;
      // hightlightEl.style.top = `-${rotateY * 70}px`;
      // hightlightEl.style.right = `${rotateX * 70}px`;
    };
    const onMouseLeave = () => {
      el.classList.add(styles.transition);
      // hightlightEl.classList.add(styles.transition);

      setTimeout(() => {
        el.style.transform = "";
        // hightlightEl.style.transform = "";
      }, 250);
      setTimeout(() => {
        el.classList.remove(styles.transition);
        // hightlightEl.classList.remove(styles.transition);
      }, 500);
    };
    el.addEventListener("mouseenter", onMouseEnter);
    el.addEventListener("mousemove", onMouseMove);
    el.addEventListener("mouseleave", onMouseLeave);
    return () => {
      el.removeEventListener("mouseenter", onMouseEnter);
      el.removeEventListener("mousemove", onMouseMove);
      el.removeEventListener("mouseleave", onMouseLeave);
    };
  }, []);
  return (
    <div className={styles.perspective}>
      <div onClick={onClick} className={styles.card} ref={ref}>
        {/* <div ref={hightlightRef} className={styles.hightlight} /> */}
        <h1>COLOR: {name}</h1>
        <svg className={styles.blob} viewBox="0 0 1 1">
          <ShapePath fill={color} />
        </svg>
      </div>
    </div>
  );
};
