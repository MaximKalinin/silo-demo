import React, { FC, useState, useEffect } from "react";

import styles from "./styles.module.less";

interface IModalProps {
  open: boolean;
  onClose: () => void;
}

export const Modal: FC<IModalProps> = (props) => {
  const { children, open, onClose } = props;
  const [internalOpen, setInternalOpen] = useState(open);
  const [internalChildren, setInternalChildren] = useState<React.ReactNode>();
  useEffect(() => {
    if (open) {
      setInternalOpen(open);
    }
  }, [open]);
  useEffect(() => {
    if (open) {
      setInternalChildren(children);
    }
  }, [open, children]);
  return internalOpen ? (
    <div className={styles.modal}>
      <div className={styles.backdrop} onClick={onClose} />
      <div
        className={`${styles.content}${open ? "" : ` ${styles.content_out}`}`}
        onAnimationEnd={() => {
          setInternalOpen(open);
        }}
      >
        {internalChildren}
      </div>
    </div>
  ) : null;
};
