import React, { FC, useEffect, useRef } from "react";
import { IPoint } from "@app/model";

export const ShapePath: FC<{ fill: string }> = ({ fill }) => {
  const pathRef = useRef<SVGPathElement>();
  useEffect(() => {
    let rendered = true;
    const pointsAmount = Math.trunc(6 + Math.random() * 4);
    const spread = 0.15;
    const points: IPoint[] = [];
    const [cx, cy] = [0.5, 0.5];
    const R = 0.5;
    const a = 0.7 / pointsAmount;
    for (let i = 0; i < pointsAmount; i++) {
      const angle = 2 * Math.PI * (i / pointsAmount);
      const sin = Math.sin(angle);
      const cos = Math.cos(angle);
      const startAngle = Math.random() * Math.PI * 2;
      const speed = 1 + Math.random() * 0.5;
      const r = getRadius({ spread, R, startAngle, speed });
      points.push({ cos, sin, r, startAngle, speed });
    }
    const update = () => {
      const el = pathRef.current;
      if (el) {
        if (!rendered) return;
        el.setAttribute("d", getPath({ points, a, cx, cy }));
      }
      points.forEach((point) => {
        point.r = getRadius({
          spread,
          R,
          startAngle: point.startAngle,
          speed: point.speed,
        });
      });
      setTimeout(update, 17);
    };
    update();
    return () => {
      rendered = false;
    };
  }, []);
  return <path ref={pathRef} fill={fill} />;
};

interface IGetPathArgs {
  points: IPoint[];
  cx: number;
  cy: number;
  a: number;
}

const getPath = ({ points, cx, cy, a }: IGetPathArgs): string =>
  points.reduce((output, { cos, sin, r }, index) => {
    const first = index === 0;
    const last = index === points.length - 1;
    const firstVal = points[0];
    return `${output}${
      first ? "M" : ` ${cx + cos * r + sin * a} ${cy - sin * r + cos * a}`
    } ${cx + cos * r} ${cy - sin * r} C ${cx + cos * r - sin * a} ${
      cy - sin * r - cos * a
    }${
      last
        ? ` ${cx + firstVal.cos * firstVal.r + firstVal.sin * a} ${
            cy - firstVal.sin * firstVal.r + firstVal.cos * a
          } ${cx + firstVal.cos * firstVal.r} ${
            cy - firstVal.sin * firstVal.r
          } Z`
        : ""
    }`;
  }, "");

interface IGetRadiusArgs {
  R: number;
  spread: number;
  speed: number;
  startAngle: number;
}

const getRadius = ({ R, spread, speed, startAngle }: IGetRadiusArgs) =>
  R -
  spread +
  Math.sin((performance.now() * speed) / 1000 + startAngle) * spread;
