import * as webpack from "webpack";
import path from "path";
import HtmlWebPackPlugin from "html-webpack-plugin";
import MiniCssExtractPlugin from "mini-css-extract-plugin";

const config: webpack.ConfigurationFactory = (_, argv) => {
  const development = argv.mode === "development";
  const htmlPlugin = new HtmlWebPackPlugin({
    react: development
      ? "https://unpkg.com/react@16/umd/react.development.js"
      : "https://unpkg.com/react@16/umd/react.production.min.js",
    reactDom: development
      ? "https://unpkg.com/react-dom@16/umd/react-dom.development.js"
      : "https://unpkg.com/react-dom@16/umd/react-dom.production.min.js",
    template: "./index.ejs",
  });
  const cssPlugin = new MiniCssExtractPlugin({ filename: "[name].[hash].css" });

  return {
    devtool: development ? "inline-source-map" : false,
    mode: development ? "development" : "production",
    entry: "./src/index",
    output: {
      path: path.resolve(__dirname, "public"),
      filename: development ? "bundle.js" : "bundle.[contenthash].js",
    },
    resolve: {
      // Add `.ts` and `.tsx` as a resolvable extension.
      extensions: [".ts", ".tsx", ".js"],
      alias: {
        "@app": path.resolve(__dirname, "src"),
      },
    },
    externals: {
      react: "React",
      "react-dom": "ReactDOM",
    },
    module: {
      rules: [
        { test: /\.tsx?$/, loader: "ts-loader" },
        {
          test: /\.less$/,
          use: [
            development
              ? { loader: "style-loader" }
              : { loader: MiniCssExtractPlugin.loader },
            {
              loader: "css-loader",
              options: {
                sourceMap: true,
                modules: {
                  mode: "local",
                  localIdentName: development
                    ? "[local]__[hash:base64]"
                    : "[hash:base64]",
                },
              },
            },
            { loader: "less-loader" },
            { loader: "postcss-loader" },
          ],
        },
      ],
    },
    plugins: [htmlPlugin, cssPlugin],
  };
};

export default config;
