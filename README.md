![logo](https://media-exp1.licdn.com/dms/image/C4E0BAQGE8SP821WxvQ/company-logo_200_200/0?e=1606348800&v=beta&t=vSH8veczqZsDwqfbZY1ZhSf5pnkKbOLamzMSt7NHfhY)
#  Silo Demo Project

## Description
This is a test project that displays colors from the endpoint below: 
`https://reqres.in/api/unknown?per_page=12`

## Access
You could access the website [🔗here🔗](https://maximkalinin.gitlab.io/silo-demo/#/).

### Prerequisites
You need NodeJs and Yarn / Npm installed to start this project on your machine.

### Launch
To launch locally, you need to clone this repo:
with ssh: `git clone git@gitlab.com:MaximKalinin/silo-demo.git`
or with https: `git clone https://gitlab.com/MaximKalinin/silo-demo.git`.

```bash
$ yarn install && yarn start
```

Then go to http://localhost:8080/ on your browser.
You should see the page👇:
![website screenshot](https://sun9-38.userapi.com/dNPxl1Dip_6_WL5TJy4_blO1ypJhHxdT80AAVw/cr24AE6FHmQ.jpg)

## Process

I started with blob generating, it was not very cool at first, but then I researched the web and found the core concept of it, so it looks more realistic now.

I had some troubles with Safari, which was not really performant doing CSS property `clip-path: url(#id);`. Then I decided to use straight svg for my blobs.

After that I added "Apple TV"-like card hover effect, which I found on the Internet initially, but then adopted to my environment. It is done using JS and CSS, so I broke the rule of assignment a little bit, but it is really cool now.

And again, I had troubles with Safari and `filter: blur();` property, so I decided to turn it off.

These were all the core steps of this task, **Thank You for reading till the end and have a great day!**
